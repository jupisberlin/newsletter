<html>
<head>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<?php include("mm.config.php"); ?>
<body>
	<div id="content">
		<div id="header">
			<span>Jupis Berlin Newsletter</span>
		</div>
		<div id="frontText" align="center">
			<img src="img/logo.png"><br>
			<!--<img src="http://img.schredder.me/500x300"><br>!-->
				<?php
					if(isset($_POST["mail"]))
					{
						if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $_POST["mail"])) {
							include("class/mailwait.class.php");
							$mW = new mailWait();
							if($mW->checkMail($_POST["mail"]))
							{
								include("class/mailman.class.php");
								$mailMan = new mailman();
								if(!$mailMan->isMember($_POST["mail"], $mm["list"], $mm["pw"], $mm["url"]))
								{
									$key = $mW->addKey($_POST["mail"]);
									$mailBody = str_replace("{{url}}", $frontend["url"]."?mail=".base64_encode($_POST["mail"])."&key=".$key, file_get_contents("mail.txt"));
									mail($_POST["mail"], "Junge Piraten Berlin Newsletter", $mailBody, "From:berlin@junge-piraten.de");
									echo "<div class='success'>E-Mail wurde erfolgreich versendet!</div>";
								}
								else
								{
									echo "<div class='error'>Die E-Mail adresse ist bereits eingetragen.</div>";
								}
							}
							else
							{
								echo "<div class='error'>An diese E-Mail adresse wurde bereits eine E-Mail verschickt.</div>";
							}
						}
						else
						{
							echo "<div class='error'>Die angegebende E-Mail adresse ist nicht gültig.</div>";
						}
					}
				?>
			Hier kannst du dich in die Newsletter der Jungen-Piraten Berlin eintragen. &Uuml;ber die Newsletter verschicken wir unregelm&auml;&szlig;ig Einladungen zu Veranstalltungen sowie Informationen &uuml;ber unsere Thematische arbeit.
		</div>
		<div id="form">
			<form method="post" action="index.php">
				<input type="text" name="mail" placeholder="E-Mail Adresse"><br>
				<input type="radio" name="action" value="add" checked="checked">Eintragen
				<!--<input type="radio" name="action" value="rem">Austragen!-->
				<br>
				<input type="submit" value="Los"><br>
			</form>
		</div>
	</div>
</body>
</html>
